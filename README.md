# Zartek Django Task LMS

**Create virtualenv by `virtualenv venv -p python3`**

* [ ] Initially create posting user followed by booking user.
    * form-data keys:
    1. name
    2. username
    3. password
    4. email
* [ ] Install requirements files by
    **`pip install -r requirements.txt`**
* [ ] Create Booking user.
* [ ] Create Luggage items by entering posting user's credentials.
* [ ] Create booking items by entering booking user's credentials.
