from django.apps import AppConfig


class LuggagesConfig(AppConfig):
    name = 'luggages'
