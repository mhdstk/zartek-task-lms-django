from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.


# class BaseModel(models.Model):
#     id = models.UUIDField(
#         primary_key=True, default=uuid.uuid4, editable=False)
#     creator = models.ForeignKey(
#         "auth.User", blank=True, related_name="creator_%(class)s_objects", on_delete=models.CASCADE)
#     updater = models.ForeignKey(
#         "auth.User", blank=True, related_name="updator_%(class)s_objects", on_delete=models.CASCADE)
#     date_added = models.DateTimeField(db_index=True, auto_now_add=True)
#     date_updated = models.DateTimeField(auto_now_add=True)
#     is_deleted = models.BooleanField(default=False)

#     class Meta:
#         abstract = True


class LuggageType(models.Model):
    name = models.CharField(max_length=128)
    rate = models.CharField(max_length=128)
    max_size = models.CharField(max_length=128, blank=True, null=True)

    def __str__(self):
        return str(self.name)


class BookLuggage(models.Model):
    volume = models.CharField(max_length=128)
    luggage_type = models.ManyToManyField(LuggageType)
    booked_items = models.CharField(max_length=256, blank=True, null=True)
    total = models.CharField(max_length=256, blank=True, null=True)
    creator = models.ForeignKey(
        "auth.User", blank=True, related_name="creator_%(class)s_objects", on_delete=models.CASCADE)

    def __str__(self):
        return str(self.name)
