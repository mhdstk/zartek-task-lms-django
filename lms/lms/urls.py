from django.conf.urls import url, include
from django.urls import path, re_path
from django.contrib import admin
from django.views.static import serve
from django.conf import settings
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),


    path('api/auth/', include(('api.v1.authentication.urls', 'api_authentication'))),
    path('api/v1/users/', include(('api.v1.users.urls', 'api_users'))),
    path('api/v1/luggages/', include(('api.v1.luggages.urls', 'api_luggages'))),

    re_path(r'^media/(?P<path>.*)/$', serve,
            {'document_root': settings.MEDIA_ROOT}),
    re_path(r'^static/(?P<path>.*)/$', serve,
            {'document_root': settings.STATIC_FILE_ROOT}),
]
