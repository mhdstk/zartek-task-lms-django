from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator
from decimal import Decimal
from versatileimagefield.fields import VersatileImageField


class BookingUser(models.Model):
    name = models.CharField(max_length=128, default="")
    phone = models.CharField(max_length=128, default="")
    address = models.CharField(max_length=128, default="")
    user = models.OneToOneField(
        "auth.User", related_name='bookinguser', blank=True, null=True, on_delete=models.CASCADE)
    is_verified = models.BooleanField(default=False)
    create_at = models.DateTimeField(db_index=True, auto_now_add=True)
    limit = models.CharField(max_length=128, blank=True, null=True)

    def __str__(self):
        return self.name


class PostingUser(models.Model):
    name = models.CharField(max_length=128, default="")
    phone = models.CharField(max_length=128, default="")
    address = models.CharField(max_length=128, default="")
    user = models.OneToOneField(
        "auth.User", related_name='postinguser', blank=True, null=True, on_delete=models.CASCADE)
    is_verified = models.BooleanField(default=False)
    create_at = models.DateTimeField(db_index=True, auto_now_add=True)

    def __str__(self):
        return self.name
