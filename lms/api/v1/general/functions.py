from django.conf import settings
from django.contrib.auth.models import User
from users.models import BookingUser, PostingUser


def generate_serializer_errors(args):
    message = ""
    for key, values in args.items():
        error_message = ""
        for value in values:
            error_message += value + ","
        error_message = error_message[:128]

        message += "%s : %s | " % (key, error_message)
    return message


def get_current_role(request):
    current_role = "user"

    if request.user.is_superuser:
        current_role = "superadmin"
    elif(BookingUser.objects.filter(user=request.user).exists()):
        current_role = "bookinguser"
    elif(PostingUser.objects.filter(user=request.user).exists()):
        current_role = "postinguser"

    return current_role
