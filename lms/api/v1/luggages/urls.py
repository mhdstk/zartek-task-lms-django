from django.urls import path, re_path
from django.conf.urls import url, include
from api.v1.luggages import views
from django.views.generic import TemplateView

app_names = ["luggages"]

urlpatterns = [
    path('create-luggage-type', views.create_luggage_type,
         name='create_luggage_type'),
    path('luggage-types', views.luggage_types,
         name='luggage_types'),
    path('create_luggage', views.create_luggage, name='create_luggage')



]
