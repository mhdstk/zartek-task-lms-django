from rest_framework import serializers
from luggages.models import BookLuggage, LuggageType


class LuggageTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = LuggageType
        fields = '__all__'


class LuggageSerializer(serializers.ModelSerializer):

    class Meta:
        model = BookLuggage
        fields = '__all__'
