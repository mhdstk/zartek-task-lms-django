from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import api_view, permission_classes, renderer_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.pagination import PageNumberPagination
from rest_framework.generics import ListAPIView
from rest_framework.authentication import TokenAuthentication
from api.v1.luggages.serializers import LuggageSerializer, LuggageTypeSerializer
from rest_framework import status
from api.v1.general.functions import generate_serializer_errors
from django.contrib.auth.models import User
from luggages.models import LuggageType, BookLuggage
from rest_framework import generics
from rest_framework import filters
from api.v1.general.functions import get_current_role
from users.models import BookingUser, PostingUser


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
def create_luggage_type(request):
    serialized = LuggageTypeSerializer(data=request.data)
    role = get_current_role(request)
    if role == "postinguser":
        if serialized.is_valid():
            serialized.save()
            response_data = {
                "StatusCode": 6000,
                "data": serialized.data
            }
            return Response(response_data, status=status.HTTP_200_OK)
        else:
            response_data = {
                "StatusCode": 6001,
                "message": generate_serializer_errors(serialized._errors)
            }
            return Response(response_data, status=status.HTTP_200_OK)
    else:
        response_data = {
            "StatusCode": 6001,
            "message": "Permission Denied"
        }
        return Response(response_data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def luggage_types(request):
    instances = LuggageType.objects.filter(is_deleted=False)

    serialized = LuggageTypeSerializer(
        instances, many=True, context={"request": request})

    response_data = {
        "StatusCode": 6000,
        'data': serialized.data
    }

    return Response(response_data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
def create_luggage(request):
    serialized = LuggageSerializer(data=request.data)
    role = get_current_role(request)
    if role == "bookinguser":
        if serialized.is_valid():
            serialized.creator = request.user
            volume = serialized.data['volume']
            # user = request.user
            # user_instence = BookingUser.objects.get(
            #     user__username=user, is_verified=True)
            # max_booking_limit = user_instence.limit
            items = request.POST['luggage_type']
            print(items)
            print("test")
            a = BookLuggage.objects.create(
                creator=request.user,
                volume=volume
            )
            print(a)
            for i in items:
                print(i)
                item = LuggageType.objects.get(pk=i)
                print(item)
                print("kooi")
                a.luggage_type.add(item)
            serialized.save()
            total = 0
            for i in all_luggages:
                item = PostLuggage.objects.get(luggage=i)
                item_price = item.luggage_cost
                total += item_price

            pk = serialized.data['id']
            instance = BookLuggage.objects.get(id=pk)
            instance.fare = total
            instance.save()
            response_data = {
                "StatusCode": 6000,
                "data": serialized.data
            }
            return Response(response_data, status=status.HTTP_200_OK)
        else:
            response_data = {
                "StatusCode": 6001,
                "message": generate_serializer_errors(serialized._errors)
            }
            return Response(response_data, status=status.HTTP_200_OK)
    else:
        response_data = {
            "StatusCode": 6001,
            "message": "This is only Available for Booking Users"
        }
        return Response(response_data, status=status.HTTP_200_OK)
