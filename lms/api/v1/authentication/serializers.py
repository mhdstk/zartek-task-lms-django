from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from users.models import BookingUser, PostingUser


class UserTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super(UserTokenObtainPairSerializer, cls).get_token(user)
        return token

    def validate(cls, attrs):
        data = super(UserTokenObtainPairSerializer, cls).validate(attrs)

        refresh = cls.get_token(cls.user)

        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)
        data['user_id'] = cls.user.id

        if cls.user.is_superuser:
            data['role'] = 'superuser'

        elif BookingUser.objects.filter(user=cls.user).exists():
            data['role'] = 'bookinguser'

        elif PostingUser.objects.filter(user=cls.user).exists():
            data['role'] = 'postinguser'

        else:
            data['role'] = 'user'

        return data
