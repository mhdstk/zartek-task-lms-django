from rest_framework import serializers
from users.models import BookingUser, PostingUser


class BookingUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = BookingUser
        fields = '__all__'


class PostingUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = PostingUser
        fields = '__all__'
