from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import api_view, permission_classes, renderer_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from api.v1.users.serializers import BookingUserSerializer, PostingUserSerializer
from rest_framework import status
from api.v1.general.functions import generate_serializer_errors
from django.contrib.auth.models import User
from users.models import PostingUser, BookingUser
from django.shortcuts import get_object_or_404


@api_view(['POST'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def create_postinguser(request):
    serialized = PostingUserSerializer(data=request.data)
    if serialized.is_valid():
        # for authentication
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']

        # Posting user details
        name = serialized.data['name']
        address = serialized.data['address']
        phone = serialized.data['phone']
        if User.objects.filter(username=username).exists():
            response_data = {
                "StatusCode": 409,
                "message": "Username already exist"
            }
            return Response(response_data, status=status.HTTP_200_OK)
        else:
            user = User.objects.create_user(
                username=username,
                first_name=name,
                email=email,
                password=password,
                is_staff=False,
            )
            # serialized.save(auto_id=auto_id,creator=user,updater=user)
            PostingUser.objects.create(
                name=name,
                user=user,
                phone=phone,
                address=address,
                is_verified=False,
            )
            response_data = {
                "StatusCode": 6000,
                "message": "Posting User Succesfully Created",
                "data": serialized.data
            }
            return Response(response_data, status=status.HTTP_200_OK)
    else:
        response_data = {
            "StatusCode": 6001,
            "message": generate_serializer_errors(serialized._errors)
        }
        return Response(response_data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def create_bookinguser(request):
    serialized = BookingUserSerializer(data=request.data)
    if serialized.is_valid():
        # for authentication
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']

        # Booking user details
        name = serialized.data['name']
        address = serialized.data['address']
        phone = serialized.data['phone']
        if User.objects.filter(username=username).exists():
            response_data = {
                "StatusCode": 409,
                "message": "Username already exist"
            }
        else:
            user = User.objects.create_user(
                username=username,
                first_name=name,
                email=email,
                password=password,
                is_staff=False,
            )
            # serialized.save(auto_id=auto_id,creator=user,updater=user)
            BookingUser.objects.create(
                name=name,
                user=user,
                phone=phone,
                address=address,
                is_verified=False,
            )
            response_data = {
                "StatusCode": 6000,
                "message": "Booking User Succesfully Created",
                "data": serialized.data
            }
        return Response(response_data, status=status.HTTP_200_OK)
    else:
        response_data = {
            "StatusCode": 6001,
            "message": generate_serializer_errors(serialized._errors)
        }
        return Response(response_data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def postingusers(request):
    instances = PostingUser.objects.all()

    if instances:
        serialized = PostingUserSerializer(
            instances, many=True, context={"request": request})

        response_data = {
            "StatusCode": 6000,
            "data": serialized.data
        }
    else:
        response_data = {
            "StatusCode": 6001,
            "message": 'PostingUser is not found'
        }
    return Response(response_data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def bookingusers(request):
    instances = BookingUser.objects.filter(is_verified=True)

    if instances:
        serialized = BookingUserSerializer(
            instances, many=True, context={"request": request})

        response_data = {
            "StatusCode": 6000,
            "data": serialized.data
        }
    else:
        response_data = {
            "StatusCode": 6001,
            "message": 'PostingUser is not found'
        }
    return Response(response_data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def get_unverified_bookingusers(request):
    instances = BookingUser.objects.filter(is_verified=False)

    if instances:
        serialized = BookingUserSerializer(
            instances, many=True, context={"request": request})

        response_data = {
            "StatusCode": 6000,
            "data": serialized.data
        }
    else:
        response_data = {
            "StatusCode": 6001,
            "message": 'PostingUser is not found'
        }
    return Response(response_data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def verify_bookinguser(request, pk):

    limit = request.POST['limit']
    instance = get_object_or_404(
        BookingUser.objects.filter(pk=pk))
    instance.is_verified = True
    instance.limit = limit
    instance.save()

    message1 = "Booking User Updated"
    message2 = " and set a limit of %s" % (limit)
    message = message1+message2
    response_data = {
        "StatusCode": 6000,
        "message": message,
    }
    return Response(response_data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def update_limit(request, pk):

    limit = request.POST['limit']
    instance = get_object_or_404(
        BookingUser.objects.filter(pk=pk))
    instance.limit = limit
    instance.save()

    message = "Set a limit of %s" % (limit)
    response_data = {
        "StatusCode": 6000,
        "message": message,
    }
    return Response(response_data, status=status.HTTP_200_OK)
