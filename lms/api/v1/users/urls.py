from django.urls import path, re_path
from django.conf.urls import url, include
from api.v1.users import views
from django.views.generic import TemplateView

app_names = ["users"]

urlpatterns = [
    # Booking users
    path('create-bookinguser', views.create_bookinguser, name='create_bookinguser'),
    path('bookingusers', views.bookingusers, name='bookingusers'),
    path('get-bookingusers-for-verification',
         views.get_unverified_bookingusers, name='get_unverified_bookingusers'),
    re_path(r'^verify-bookinguser/(?P<pk>.*)/$',
            views.verify_bookinguser, name='verify_bookinguser'),
    re_path(r'^update-limit/(?P<pk>.*)/$',
            views.update_limit, name='update_limit'),

    # Posting users
    path('create-postinguser', views.create_postinguser, name='create_postinguser'),
    path('postingusers', views.postingusers, name='postingusers'),

]
